<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>APT</title>
    
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/self.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" href="css/style.css" />
    <script type="text/javascript">
    </script>
    <style>
    </style>
  </head>
  <body>
      <div class="container header">
          <div>
          </div>
      </div>
      <article class="container">
          <div class="col-md-8 border1">
              <div class="border8">
	              	<div class="border9">
	              	    <c:forEach var="User" items="${divideUsers[0]}" varStatus="status">
	                    <div class="border10">
	                      <div class="pimage" id="${User.y}${User.x}" onclick="javascript:checkWindow(this);"
	                      data-userId="${User.id}" data-x="${status.count}" data-y=5>
                          <c:if test="${User.id ne 0}">
                            <img src="image/noimage.png" height="130" width="124">
                          </c:if>
	                      </div>
	                    </div>
	                    </c:forEach>
	              	</div>
	                <div class="border9">
	              	    <c:forEach var="User" items="${divideUsers[1]}" varStatus="status">
	                    <div class="border10">
	                      <div class="pimage" id="${User.y}${User.x}" onclick="javascript:checkWindow(this);"
	                      data-userId="${User.id}" data-x="${status.count}" data-y=4>
                          <c:if test="${User.id ne 0}">
                            <img src="image/noimage.png" height="130" width="124">
                          </c:if>
	                      </div>
	                    </div>
	                    </c:forEach>
	              	</div>
	              	<div class="border9">
	              	    <c:forEach var="User" items="${divideUsers[2]}" varStatus="status">
	                    <div class="border10">
	                      <div class="pimage" id="${User.y}${User.x}" onclick="javascript:checkWindow(this);"
	                      data-userId="${User.id}" data-x="${status.count}" data-y=3>
                          <c:if test="${User.id ne 0}">
                            <img src="image/noimage.png" height="130" width="124">
                          </c:if>
	                      </div>
	                    </div>
	                    </c:forEach>
	              	</div>
	              	<div class="border9">
	              	    <c:forEach var="User" items="${divideUsers[3]}" varStatus="status">
	                    <div class="border10">
	                      <div class="pimage" id="${User.y}${User.x}" onclick="javascript:checkWindow(this);"
	                      data-userId="${User.id}" data-x="${status.count}" data-y=2>
                          <c:if test="${User.id ne 0}">
                            <img src="image/noimage.png" height="130" width="124">
                          </c:if>
	                      </div>
	                    </div>
	                    </c:forEach>
	              	</div>
	              	<div class="border9">
	              	    <c:forEach var="User" items="${divideUsers[4]}" varStatus="status">
	                    <div class="border10">
	                      <div class="pimage" id="${User.y}${User.x}" onclick="javascript:checkWindow(this);"
	                      data-userId="${User.id}" data-x="${status.count}" data-y=1>
                          <c:if test="${User.id ne 0}">
                            <img src="image/noimage.png" height="130" width="124">
                          </c:if>
	                      </div>
	                    </div>
	                    </c:forEach>
	              	</div>
              </div>
              <div class="border9">
                  <div class="bimage"><img src="logo.jpg"></div>
              </div>
          </div>
          <div class="col-md-4 border2">
              <div class="border3">
                  <div class="apttitle">
                    <div class="aptname">${apt.name }</div>
                  </div>
                  <div class="loctitle">
                    <div class="col-md-9">
                        <div class="pname">${loginUserName }</div>
                    </div>
                    <div class="settingdiv">
                        <button class="setting" onclick="location.href='/goInfoPage.do'">정보수정</button>
                        <button class="setting" onclick="javascript:outUserInApt();">탈퇴</button>
                    </div>
                </div>
              <div class="border4">
              </div>
              <div class="border5">
                  <div class="col-md-10 border6">
                    <textarea rows="5" cols="35" style="resize: none;">메시지 작성하는 곳</textarea>
                  </div>
                  <div class="border6">                    
                      <button class="msgbutton">전송</button>
                  </div>
              </div>
              </div>
          </div>
      </article>
      <br>
      <div class="container footer">
          <div></div>
      </div>
      <div class="modal fade" id="infoform" tabindex="-1" role="dialog" aria-labelledby="messageModalTitle"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <span class="form_title1 text_center"><h3>501호</h3></span>
                    </div>
                    <form action="/message" method="post" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="center">
                                    <div class="infoimage">
                                        <img src="image/noimage.png" height="400" width="400">
                                    </div>
                                    <div class="infotext">
                                        <textarea rows="5" cols="56" style="resize: none;">501에 입주한 사람이에요.
                                        </textarea>
                                    </div>  
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer pb-0">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><h4>닫기</h4></button>
                        </div>
                    </form>
                </div>
            </div>        </div>
  </body>
</html>