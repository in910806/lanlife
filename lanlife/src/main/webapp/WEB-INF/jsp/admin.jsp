<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>APT</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" href="css/style.css" />
    <script type="text/javascript" src="js/self.js"></script>
    <style>
    </style>
  </head>
  <body>
      <div class="container header">
          <div></div>
      </div>
      <article class="container">
          <div class='col-md-6 left'>
              <div class='up'>
                  <div class='upwrap1'>
                      <h3>APT관리</h3>
                  </div>
                  <div class='upwrap2'>
                    <div class='col-md-4'>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createapt">APT생성하기</button>
                    </div>
                        <form role="form" id="form-buscar">
                        <div class="form-group">
                        <div class="input-group">
                        <input id="aptSearchText" class="form-control" type="text" name="aptSearchText" placeholder="Search..." required/>
                        <span class="input-group-btn">
                        <button class="btn btn-success" onclick="location.href='/aptSearch.do'">
                        <i class="glyphicon glyphicon-search" aria-hidden="true"></i> 검색
                        </button>
                        </span>
                        </div>
                        </div>
                        </form>
                </div>
              </div>
              <div class='middle'>
                  <table class="table ptable">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">APTID</th>
                          <th scope="col">APT이름</th>
                          <th scope="col">카페코드</th>
                          <th scope="col">정원</th>
                          <th scope="col">삭제</th>
                        </tr>
                      </thead>
                      <tbody>
                      <c:forEach var="Apt" items="${Apts}" varStatus="status">
                        <tr>
                          <th scope="row">${status.count}</th>
                          	<td>
                          	<c:if test="${Apt.cafeId ne null}">
                          	${Apt.id}
                          	</c:if>
                          	</td>
                          <td>${Apt.name}</td>
                          <td>${Apt.cafeId}</td>
                        	<td>
                        	<c:if test="${Apt.cafeId ne null}">
                        	${fn:length(Apt.users)}/30
                        	</c:if>
                        	</td>
                          	<td>
                          	<c:if test="${Apt.cafeId ne null}">
                          	<button onclick="javascript:deleteAdminApt(this.id);" id="${Apt.id}">삭제</button>
                          	</c:if>
                          	</td>
                        </tr>
                        </c:forEach>
                      </tbody>
                    </table>
              </div>
              <div class='down'>
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
          </div>
          <div class='col-md-6 right'>
              <div class='up'>
                  <div class='upwrap1'>
                      <h3>회원관리</h3>
                  </div>
                  <div class='upwrap2'>
                    <form role="form" id="form-buscar">
                    <div class="form-group">
                    <div class="input-group">
                    <input id="userSearchText" class="form-control" type="text" name="userSearchText" placeholder="Search..." required/>
                    <span class="input-group-btn">
                    <button class="btn btn-success" type="submit">
                    <i class="glyphicon glyphicon-search" aria-hidden="true"></i> 검색
                    </button>
                    </span>
                    </div>
                    </div>
                    </form>
                </div>
              </div>
              <div class='middle'>
                  <table class="table ptable">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">아이디</th>
                          <th scope="col">카페코드</th>
                          <th scope="col">APT코드</th>
                          <th scope="col">X</th>
                          <th scope="col">Y</th>
                          <th scope="col">입주일</th>
                          <th scope="col">강퇴</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td>ck_blog</td>
                          <td>131313</td>
                          <td>A000001</td>
                          <td>5</td>
                          <td>1</td>
                          <td>18/8/22</td>
                          <td><button>강퇴</button></td>
                        </tr>
                        <tr>
                          <th scope="row">2</th>
                          <td>ck_blog</td>
                          <td>131313</td>
                          <td>A000001</td>
                          <td>5</td>
                          <td>1</td>
                          <td>18/8/22</td>
                          <td><button>강퇴</button></td>
                        </tr>
                        <tr>
                          <th scope="row">3</th>
                          <td>ck_blog</td>
                          <td>131313</td>
                          <td>A000001</td>
                          <td>5</td>
                          <td>1</td>
                          <td>18/8/22</td>
                          <td><button>강퇴</button></td>
                        </tr>
                          <tr>
                          <th scope="row">4</th>
                          <td>ck_blog</td>
                          <td>131313</td>
                          <td>A000001</td>
                          <td>5</td>
                          <td>1</td>
                          <td>18/8/22</td>
                          <td><button>강퇴</button></td>
                        </tr>
                         <tr>
                          <th scope="row">5</th>
                          <td>ck_blog</td>
                          <td>131313</td>
                          <td>A000001</td>
                          <td>5</td>
                          <td>1</td>
                          <td>18/8/22</td>
                          <td><button>강퇴</button></td>
                        </tr>
                          <tr>
                          <th scope="row">6</th>
                          <td>ck_blog</td>
                          <td>131313</td>
                          <td>A000001</td>
                          <td>5</td>
                          <td>1</td>
                          <td>18/8/22</td>
                          <td><button>강퇴</button></td>
                        </tr>
                          <tr>
                          <th scope="row">7</th>
                          <td>ck_blog</td>
                          <td>131313</td>
                          <td>A000001</td>
                          <td>5</td>
                          <td>1</td>
                          <td>18/8/22</td>
                          <td><button>강퇴</button></td>
                        </tr>
                          <tr>
                          <th scope="row">8</th>
                          <td>ck_blog</td>
                          <td>131313</td>
                          <td>A000001</td>
                          <td>5</td>
                          <td>1</td>
                          <td>18/8/22</td>
                          <td><button>강퇴</button></td>
                        </tr>
                          <tr>
                          <th scope="row">9</th>
                          <td>ck_blog</td>
                          <td>131313</td>
                          <td>A000001</td>
                          <td>5</td>
                          <td>1</td>
                          <td>18/8/22</td>
                          <td><button>강퇴</button></td>
                        </tr>
                          <tr>
                          <th scope="row">10</th>
                          <td>ck_blog</td>
                          <td>131313</td>
                          <td>A000001</td>
                          <td>5</td>
                          <td>1</td>
                          <td>18/8/22</td>
                          <td><button>강퇴</button></td>
                        </tr>
                          <tr>
                          <th scope="row">11</th>
                          <td>ck_blog</td>
                          <td>131313</td>
                          <td>A000001</td>
                          <td>5</td>
                          <td>1</td>
                          <td>18/8/22</td>
                          <td><button>강퇴</button></td>
                        </tr>
                          <tr>
                          <th scope="row">12</th>
                          <td>ck_blog</td>
                          <td>131313</td>
                          <td>A000001</td>
                          <td>5</td>
                          <td>1</td>
                          <td>18/8/22</td>
                          <td><button>강퇴</button></td>
                        </tr>
                          <tr>
                          <th scope="row">13</th>
                          <td>ck_blog</td>
                          <td>131313</td>
                          <td>A000001</td>
                          <td>5</td>
                          <td>1</td>
                          <td>18/8/22</td>
                          <td><button>강퇴</button></td>
                        </tr>
                          <tr>
                          <th scope="row">14</th>
                          <td>ck_blog</td>
                          <td>131313</td>
                          <td>A000001</td>
                          <td>5</td>
                          <td>1</td>
                          <td>18/8/22</td>
                          <td><button>강퇴</button></td>
                        </tr>
                          <tr>
                          <th scope="row">15</th>
                          <td>ck_blog</td>
                          <td>131313</td>
                          <td>A000001</td>
                          <td>5</td>
                          <td>1</td>
                          <td>18/8/22</td>
                          <td><button>강퇴</button></td>
                        </tr>
                      </tbody>
                    </table>
              </div>
              <div class='down'>
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
          </div>
      </article>
      <div class="container footer">
          <div></div>
      </div>
      
      <div class="modal fade" id="createapt" tabindex="-1" role="dialog" aria-labelledby="messageModalTitle"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <span class="form_title1"> APT 생성하기 </span>
                    </div>
                    <form action="/aptCreate.do" method="post" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="form-group">
                                    <input type="text" class="form-control" id="cafeId" name="cafeId" placeholder="카페코드">
                                    <br>
                                    <input type="text" class="form-control" id="aptName" name="aptName" placeholder="APT이름">
                            </div>
                        </div>
                        <div class="modal-footer pb-0">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
                            <button type="submit" class="btn btn-primary text-white font-weight-bold">생성</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
  </body>
</html>