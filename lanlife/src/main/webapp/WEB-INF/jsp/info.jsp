<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>APT</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" href="css/style.css" />
    <script type="text/javascript" src="js/self.js"></script>
    <style>
    </style>
  </head>
  <body>
      <div class="container header">
          <div></div>
      </div>
      <article class="container">
          <div>
              <div class='infotitle'>
                   	회원 정보 수정
              </div>
              <div class='inputpw'>
                    <img src="noimage.png" width="400px" height='400px'>
                    <br>
                    <br>
                    <textarea class='infoinput' cols='56' rows='5'>${user.content }</textarea>
                    <br>
                    <span class="btn">
                    <button class="btn infobtnsize" type="submit">
                    취소
                    </button>
                    <button class="btn btn-success infobtnsize" type="submit">
                    수정
                    </button>
                  </span>
             </div>
          </div>
      </article>
      <div class="container footer">
          <div></div>
      </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="resource/js/bootstrap.min.js"></script>
  </body>
</html>