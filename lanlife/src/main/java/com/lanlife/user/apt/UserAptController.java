package com.lanlife.user.apt;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.lanlife.domain.Apt;
import com.lanlife.domain.User;
import com.lanlife.user.apt.service.UserAptService;

@Controller
public class UserAptController {
	
	
	@Resource(name="userAptService")
	private UserAptService userAptService;
	
	@RequestMapping(value="/registerUerinApt.do", method = RequestMethod.GET)
	private ModelAndView registerUserinApt (HttpServletRequest request,
            HttpServletResponse response) {
		
//		String userId = (String)request.getParameter("loginUserId");
//		String aptId = (String)request.getParameter("loginAptId");
		HttpSession session = request.getSession();
		String userId = Integer.toString((Integer)session.getAttribute("loginUserId"));
		String aptId = (String)session.getAttribute("loginAptId");
		System.out.println("getLoginUser = " + userId);
		//
		int x = Integer.parseInt(request.getParameter("x"));
		int y = Integer.parseInt(request.getParameter("y"));
		
		int check = userAptService.joinApt(userId, Integer.parseInt(aptId), x, y);
		//
		ModelAndView mav = new ModelAndView();
		List<User> users = userAptService.searchAptUserByAptId(Integer.parseInt(aptId));
		List<List<User>> divideUsers = userAptService.divideUserList(users);
		Apt apt = userAptService.searchAptById(Integer.parseInt(aptId));
		String loginUserName = userAptService.checkloginUserInApt(users, Integer.parseInt(userId));
		mav.addObject("loginUserName", loginUserName);
		mav.addObject("apt", apt);
		mav.addObject("divideUsers", divideUsers);
		mav.setViewName("main");
		return mav;
	}
	
	@RequestMapping(value="/aptMain.do")	
	private ModelAndView searchUserinApt (HttpServletRequest request,
            HttpServletResponse response) {
		
		
		ModelAndView mav = new ModelAndView();
		List<User> users = userAptService.searchAptUserByAptId(1);
		List<User> users1 = new ArrayList<>();
		List<User> users2 = new ArrayList<>();
		List<User> users3 = new ArrayList<>();
		List<User> users4 = new ArrayList<>();
		List<User> users5 = new ArrayList<>();
		
		for(int i=0; i<users.size(); i++) {
			if((i/5)==0) {
				users1.add(users.get(i));
			}else if((i/5)==1) {
				users2.add(users.get(i));
			}else if((i/5)==2) {
				users3.add(users.get(i));
			}else if((i/5)==3) {
				users4.add(users.get(i));
			}else if((i/5)==4) {
				users5.add(users.get(i));
			}
		}
		
		mav.addObject("users1", users1);
		mav.addObject("users2", users2);
		mav.addObject("users3", users3);
		mav.addObject("users4", users4);
		mav.addObject("users5", users5);
		mav.setViewName("main");
		return mav;
	}
	
	@RequestMapping(value="/outUserInApt.do")
	private ModelAndView outUserInApt (HttpServletRequest request,
            HttpServletResponse response) {
		
		HttpSession session = request.getSession();
		String userId = Integer.toString((Integer)session.getAttribute("loginUserId"));
		String aptId = (String)session.getAttribute("loginAptId");
		
		userAptService.outApt(userId);
		
		ModelAndView mav = new ModelAndView();
		List<User> users = userAptService.searchAptUserByAptId(Integer.parseInt(aptId));
		List<List<User>> divideUsers = userAptService.divideUserList(users);
		Apt apt = userAptService.searchAptById(Integer.parseInt(aptId));
		String loginUserName = userAptService.checkloginUserInApt(users, Integer.parseInt(userId));
		mav.addObject("loginUserName", loginUserName);
		mav.addObject("apt", apt);
		mav.addObject("divideUsers", divideUsers);
		mav.setViewName("main");
		return mav;
	}
	
	@RequestMapping(value="/goInfoPage.do")
	private ModelAndView goInfoPage (HttpServletRequest request,
            HttpServletResponse response) {
		
		HttpSession session = request.getSession();
		String userId = Integer.toString((Integer)session.getAttribute("loginUserId"));
		
		User user = userAptService.searchUserById(Integer.parseInt(userId));
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("user", user);
		mav.setViewName("info");
		return mav;
	}

}
