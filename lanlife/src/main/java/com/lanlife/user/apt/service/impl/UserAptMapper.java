package com.lanlife.user.apt.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.lanlife.domain.Apt;
import com.lanlife.domain.User;

@Mapper
public interface UserAptMapper {
	
	public List<User> searchAptUserByAptId(int aptId);
	public int joinApt(Map<String, String> joinInfo);
	public int outApt(String userId);
	public Apt searchAptById(int aptId);
	public User searchUserById(int userId);

}
