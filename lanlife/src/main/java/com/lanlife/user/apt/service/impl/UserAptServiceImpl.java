package com.lanlife.user.apt.service.impl;

import java.util.List;

import com.lanlife.domain.Apt;
import com.lanlife.domain.User;

public interface UserAptServiceImpl {
	
	//1. 회원정보 수정 (이미지 포함)
	
	//2. 회원 상세정보 보여주기
	
	//3. APT 입주
	public int joinApt(String userId, int aptId, int x, int y);
	//4. APT 이사
	
	//5. APT 탈퇴
	public int outApt(String userId);
	
	//6. 입주 리스트 가져오기
	public List<User> searchAptUserByAptId(int aptId);
	
	//7. APT정보 가져오기
	public Apt searchAptById(int aptId);
	
	//8. 회원정보 가져오기
	public User searchUserById(int userId);

}
