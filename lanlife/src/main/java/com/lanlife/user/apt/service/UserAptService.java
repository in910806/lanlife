package com.lanlife.user.apt.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.lanlife.domain.Apt;
import com.lanlife.domain.User;
import com.lanlife.user.apt.service.impl.UserAptMapper;
import com.lanlife.user.apt.service.impl.UserAptServiceImpl;

@Service
public class UserAptService implements UserAptServiceImpl {
	
	@Resource(name="userAptMapper")
	private UserAptMapper userAptMapper;
	
	public static final int MAXUSER = 25;

	@Override
	public List<User> searchAptUserByAptId(int aptId) {
		
		List<User> userList = new ArrayList<>();
		List<User> displayUsers = new ArrayList<>();
		
		try {
			userList = userAptMapper.searchAptUserByAptId(aptId);
			for(int i=0; i < MAXUSER; i++) {
				if(userList.size()-1 < i) {
					User user = new User();
					user.setId(0);
					userList.add(i, user);
				}
			}
			displayUsers = displayUsersForApt(userList);
			for(int i=0; i < displayUsers.size(); i++) {
				System.out.println("list = " + i + " user = " + displayUsers.get(i).getId());
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return displayUsers;
	}
	
	public List<User> displayUsersForApt(List<User> userList){
		
		List<User> displayUsers = new ArrayList<>();
		for(int i=0; i < MAXUSER; i++) {
			if(displayUsers.size()-1 < i) {
				User user = new User();
				displayUsers.add(i, user);
			}
		}
		
		try {
			for(int i=0; i<userList.size(); i++) {
				User user = userList.get(i);
				if(user.getId() != 0) {
					int order = (5-user.getY())*5;
					displayUsers.remove(order+user.getX()-1);
					displayUsers.add(order+user.getX()-1, user);
				}
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return displayUsers;
	}
	
	public List<List<User>> divideUserList (List<User> userList){
		
		List<List<User>> list = new ArrayList<>();
		
		List<User> users1 = new ArrayList<>();
		List<User> users2 = new ArrayList<>();
		List<User> users3 = new ArrayList<>();
		List<User> users4 = new ArrayList<>();
		List<User> users5 = new ArrayList<>();
		
		for(int i=0; i<userList.size(); i++) {
			if((i/5)==0) {
				users1.add(userList.get(i));
				
			}else if((i/5)==1) {
				users2.add(userList.get(i));
				
			}else if((i/5)==2) {
				users3.add(userList.get(i));
				
			}else if((i/5)==3) {
				users4.add(userList.get(i));
				
			}else if((i/5)==4) {
				users5.add(userList.get(i));
				
			}
		}
		list.add(users1);
		list.add(users2);
		list.add(users3);
		list.add(users4);
		list.add(users5);
		return list;
	}

	@Override
	public int joinApt(String userId, int aptId, int x, int y) {
		int check = 0;
		try {
			Map<String, String> joinInfo = new HashMap<String, String>();
			joinInfo.put("userId", userId);
			joinInfo.put("aptId", Integer.toString(aptId));
			joinInfo.put("x", Integer.toString(x));
			joinInfo.put("y", Integer.toString(y));
			check = userAptMapper.joinApt(joinInfo);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return check;
	}

	@Override
	public int outApt(String userId) {
		int check = 0;
		check = userAptMapper.outApt(userId);
		return check;
	}

	@Override
	public Apt searchAptById(int aptId) {
		Apt apt = new Apt();
		apt = userAptMapper.searchAptById(aptId);
		return apt;
	}
	
	public String checkloginUserInApt(List<User> userList, int loginUserId) {
		
		String loginUserName = "방문객";
		
		for(int i=0; i<userList.size(); i++) {
			User user = userList.get(i);
			if(user.getId() == loginUserId) {
				int x = user.getX();
				int y = user.getY();
				loginUserName = Integer.toString(y) + "0" + Integer.toString(x) + "호";
			}
		}
		return loginUserName;
	}

	@Override
	public User searchUserById(int userId) {
		
		User user = userAptMapper.searchUserById(userId);
		
		return user;
	}
	
}
