package com.lanlife.user.main.service;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lanlife.domain.Apt;
import com.lanlife.domain.User;

public interface MainService {

	User callback(Map<String, Object> map) throws Exception;

	void signupNaverId(User userInfo) throws Exception;

	Apt getApdId(String aptId) throws Exception;

	boolean getClubId(String preurl, String string) throws Exception;

	void goPreUrl(HttpServletResponse response, String preurl, String errMsg) throws Exception;

	User setUserInfo(Map<String, Object> map) throws Exception;

	Map<String, Object> changeJsonToMap(String code, String state, HttpSession session) throws Exception;

}
