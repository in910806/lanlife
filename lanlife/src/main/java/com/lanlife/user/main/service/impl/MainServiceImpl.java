package com.lanlife.user.main.service.impl;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.lanlife.domain.Apt;
import com.lanlife.domain.User;
import com.lanlife.user.main.NaverLoginBO;
import com.lanlife.user.main.service.MainService;

@Service("mainService")
public class MainServiceImpl implements MainService{
	
	@Resource(name="mainMapper")
	private MainMapper mainMapper;

	@Override
	public User callback(Map<String, Object> map) throws Exception {
		
		List<User> userInfo = mainMapper.getExistId(map.get("id").toString());
		
		User user = new User();
		
		if (userInfo.size() > 0) {
			user = userInfo.get(0);
		}
		
		return user;
	}

	@Override
	public void signupNaverId(User userInfo) throws Exception {
		mainMapper.signupNaverId(userInfo);
	}

	@Override
	public Apt getApdId(String aptId) throws Exception {
		return mainMapper.getApdId(aptId);
	}
	
	public void goPreUrl(HttpServletResponse response, String preurl, String errMsg) throws Exception {
		response.setContentType("text/html; charset=utf-8");
		
		PrintWriter out = response.getWriter();
		
		out.println("<script language='javascript'>");
		out.println("alert('"+errMsg+"');");
		out.println("location.href='"+preurl+"'");
		out.println("</script>");
		
		out.flush();
	}
	
	public boolean getClubId (String preurl, String dbClubId) throws Exception {
		Boolean result = false;
		
		Document doc = Jsoup.connect(preurl).get();
		
		// club id가 명기된 element 선택
		Elements element = doc.select("#cafeApply_check input[name=clubid]");
		
		// 해당 element의 club id value 가져오기
		String clubId = element.attr("value");
		
		if (dbClubId.equals(clubId)) {
			result = true;
		}
		
		return result;
	}
	
	@Override
	public User setUserInfo(Map<String, Object> map) throws Exception {
		
		User user = new User();
		
		if (map.size() > 0) {
			user.setId(Integer.parseInt(map.get("id").toString()));
			user.setEmail(map.get("email").toString());
		}
		
		return user;
	}

	@Override
	public Map<String, Object> changeJsonToMap(String code, String state, HttpSession session) throws Exception {
		OAuth2AccessToken oauthToken = NaverLoginBO.getAccessToken(session, code, state);
		
		String apiResult = NaverLoginBO.getUserProfile(oauthToken);
		
		ObjectMapper mapper = new ObjectMapper();
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		apiResult = apiResult.substring(apiResult.indexOf("\"response\":")+11);
		
		apiResult = apiResult.substring(0, apiResult.length()-1);
		
		map = mapper.readValue(apiResult, new TypeReference<Map<String, String>>(){});
		
		return map;
	}
	
}
