package com.lanlife.user.main.service.impl;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.lanlife.domain.Apt;
import com.lanlife.domain.User;

@Mapper
public interface MainMapper {

	List<User> getExistId(String id);

	void signupNaverId(User userInfo);

	Apt getApdId(String aptId);

}
