package com.lanlife.user.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.lanlife.domain.Apt;
import com.lanlife.domain.User;
import com.lanlife.user.apt.service.UserAptService;
import com.lanlife.user.main.service.MainService;

@Controller
public class MainController {
	
	private String aptId = "";
	
	@Resource(name="mainService")
	private MainService mainService;
	
	@Resource(name="userAptService")
	private UserAptService userAptService;
	
	/* NaverLoginBO */
	@Autowired
	private NaverLoginBO naverLoginBO;

	/* NaverLoginBO */
	@SuppressWarnings("unused")
	private void setNaverLoginBO(NaverLoginBO naverLoginBO){
		this.naverLoginBO = naverLoginBO;
	}
	
	//Test
	@RequestMapping(value="/")
	public String test() throws Exception {
		return "index";
	}
	
	@RequestMapping(value="/aptId/{aptId}", method = RequestMethod.GET)
    public String main(@PathVariable("aptId") String aptId, HttpSession session, HttpServletRequest request, HttpServletResponse response) throws Exception {
       
		String preurl = request.getHeader("referer");
		
		setAptId(aptId);
        
		//1. 네이버 카페의 링크 클릭 / apt 존재 확인
    	Apt apt = mainService.getApdId(aptId);
    	
    	
    	//1.1 APT ID 값이 있으면 2번 항목으로 이동
    	if (apt != null) {
    		if (apt.getId() != null && apt.getId() != "NONE") {
    			//2. Club Id 비교 (Crawling)
    			//2.1 정상 접근일 시 naver login
    			if (mainService.getClubId(preurl, apt.getCafeId())) {
    				/* 네아로 인증 URL을 생성하기 위하여 getAuthorizationUrl을 호출 */
    		        String naverAuthUrl = naverLoginBO.getAuthorizationUrl(session);
    		        
    		        /* 생성한 인증 URL을 View로 전달 */
    		        return "redirect:" + naverAuthUrl;
    			//2.2 비정상 루트 접근 시 (이전 url로 이동 alert 창 띄우기)
    			} else {
    				String errMsg = "비정상 접근입니다. Naver Cafe에서 접근 해야합니다.";
    				
    				mainService.goPreUrl(response, preurl, errMsg);
    			}
    		}
    		
    	//1.2 APT ID 조회가 안되면 Alert 창 띄우고 기존 URL 이동
    	} else {
    		String errMsg = "개설된 아파트가 존재하지 않습니다.";
			
			mainService.goPreUrl(response, preurl, errMsg);
    	}
    	
    	return "";
	}
	
	public String naverLogin(HttpSession session, HttpServletRequest request) throws Exception {
		/* 네아로 인증 URL을 생성하기 위하여 getAuthorizationUrl을 호출 */
        String naverAuthUrl = naverLoginBO.getAuthorizationUrl(session);
        
        /* 생성한 인증 URL을 View로 전달 */
        return "redirect:" + naverAuthUrl;
	}
	
	@RequestMapping(value="/callback")
	public String callback(@RequestParam String code, @RequestParam String state, HttpSession session, ModelMap model) throws Exception {
		String callbackurl = "main";
		
		//네이버 로그인 이후 return json값 map으로 변경
		Map<String, Object> map = mainService.changeJsonToMap(code, state, session);
		
		//해당 user가 db에 가입되어 있는지 조회
		User userInfo = mainService.callback(map);
		
		//db 조회값이 없다면 
		if (userInfo.getId() <= 0) {
			//해당 user 정보 셋팅 후 db에 저장 (id, email만 저장함)
			userInfo = mainService.setUserInfo(map);
			mainService.signupNaverId(userInfo);
		}
		

		
		//추후 main.jsp에 필요한 값 추가 해야함 (aptId, userId, userContent, apt가입유무 등이 필요할것으로 보여짐)
//		model.addAttribute("aptId", this.getAptId());
//		model.addAttribute("userId", userInfo.getId());

		
		//login 유져아이디 저장
//		model.addAttribute("loginAptId", this.getAptId());
//		model.addAttribute("loginUserId", userInfo.getId());
		session.setAttribute("loginUserId", userInfo.getId());
		session.setAttribute("loginAptId", this.getAptId());
		//메인에 가져가는 것들
		List<User> users = userAptService.searchAptUserByAptId(Integer.parseInt(this.getAptId()));
		List<List<User>> divideUsers = userAptService.divideUserList(users);
		Apt apt = userAptService.searchAptById(Integer.parseInt(this.getAptId()));
		String loginUserName = userAptService.checkloginUserInApt(users, userInfo.getId());
		model.addAttribute("loginUserName", loginUserName);
		model.addAttribute("apt", apt);
		model.addAttribute("divideUsers", divideUsers);
		
		return callbackurl;
	}
	
	private String getAptId() {
		return this.aptId;
	}
	
	private void setAptId (String str) {
		this.aptId = str;
	}
}
