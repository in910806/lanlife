package com.lanlife.admin.apt.service.impl;

import java.util.List;

import com.lanlife.domain.Apt;
import com.lanlife.domain.User;

public interface AdminAptServiceImpl {
	
	public int createAdminApt(Apt apt);
	public int deleteAdminAptById(String id);
	public List<Apt> searchAdminApt(int pageNum);
	public List<Apt> searchAdminAptByName(String aptName);
	public List<User> searchAdminUserByAptId(String aptId);

}
