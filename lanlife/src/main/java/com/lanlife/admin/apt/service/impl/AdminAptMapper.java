package com.lanlife.admin.apt.service.impl;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.lanlife.domain.Apt;
import com.lanlife.domain.User;

@Mapper
public interface AdminAptMapper {
	
	public int createAdminApt(Apt apt);
	public List<Apt> searchAdminApt(int pageNum);
	public int deleteAdminAptById(String id);
	public List<Apt> searchAdminAptByName(String aptName);
	public List<User> searchAdminUserByAptId(String aptId);

}
