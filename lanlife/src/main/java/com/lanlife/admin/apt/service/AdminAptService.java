package com.lanlife.admin.apt.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.lanlife.admin.apt.service.impl.AdminAptMapper;
import com.lanlife.admin.apt.service.impl.AdminAptServiceImpl;
import com.lanlife.domain.Apt;
import com.lanlife.domain.User;

@Service
public class AdminAptService implements AdminAptServiceImpl {
	
	public static final int MAXROW = 15; 
	
	@Resource(name="adminAptMapper")
	private AdminAptMapper adminAptMapper;


	@Override
	public int createAdminApt(Apt apt) {

		int check = 0;
		try {
			System.out.println(apt.toString());
			check = adminAptMapper.createAdminApt(apt);
		} catch (Exception e) {
			e.getMessage();
		}
		return check;
	}

	@Override
	public int deleteAdminAptById(String id) {
		int check = 0;

		check = adminAptMapper.deleteAdminAptById(id);
		
		return check;
	}

	@Override
	public List<Apt> searchAdminApt(int pageNum) {
		
		List<Apt> list = new ArrayList<>();
		
		try {
			System.out.println(pageNum);
			list = adminAptMapper.searchAdminApt(pageNum);
			
			for(int i=0; i<list.size(); i++) {
				String aptNum = list.get(i).getId();
				list.get(i).setUsers(searchAdminUserByAptId(aptNum));
			}
			
    		for(int i=0; i<list.size(); i++) {
    			Apt apt = list.get(i);
    			System.out.println("aptId = " + apt.getId() + " aptName = "+ apt.getName() + " cafeId = " + apt.getCafeId());
    		}
    		
    		for(int j = list.size(); j < MAXROW; j++) {
    			Apt apt = new Apt();
    			list.add(apt);
    		}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return list;
	}

	@Override
	public List<Apt> searchAdminAptByName(String aptName) {
		List<Apt> list = new ArrayList<>();
		try {
			list = adminAptMapper.searchAdminAptByName(aptName);
			System.out.println("list.size() = " + list.size());
			for(int i=0; i<list.size(); i++) {
				String aptNum = list.get(i).getId();
				list.get(i).setUsers(searchAdminUserByAptId(aptNum));
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return list;
	}

	@Override
	public List<User> searchAdminUserByAptId(String aptId) {
		
		List<User> list = new ArrayList<>();
		try {
			System.out.println("유져가져오기");
			list = adminAptMapper.searchAdminUserByAptId(aptId);
			System.out.println("list.size() = " + list.size());
			for(int i = 0; i < list.size(); i++) {
				System.out.println(list.get(i).getAptId());
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return list;
	}
	
}
