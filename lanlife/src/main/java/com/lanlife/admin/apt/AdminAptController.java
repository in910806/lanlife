package com.lanlife.admin.apt;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.lanlife.admin.apt.service.AdminAptService;
import com.lanlife.admin.apt.service.impl.AdminAptServiceImpl;
import com.lanlife.domain.Apt;

@Controller
public class AdminAptController {
	
	@Resource(name="adminAptService")
	private AdminAptServiceImpl adminAptService;
	
	@RequestMapping(value="/admin")
	private ModelAndView adminPage (HttpServletRequest request,
            HttpServletResponse response){
		ModelAndView mav = new ModelAndView();
		List<Apt> list = adminAptService.searchAdminApt(1);
    	mav.setViewName("admin");
    	mav.addObject("Apts", list);
    	return mav;
	}
	
	
	@RequestMapping(value="/readApt.do")
	private ModelAndView readApt (HttpServletRequest request,
            HttpServletResponse response){
		
		List<Apt> list = adminAptService.searchAdminApt(1);
//		for(int i=0; i<list.size(); i++) {
//			Apt apt = list.get(i);
//			System.out.println("aptId = " + apt.getId() + " aptName = "+ apt.getName() + " cafeId = " + apt.getCafeId());
//		}
		
    	ModelAndView mav = new ModelAndView();
    	mav.setViewName("admin");
    	mav.addObject("Apts", list);
		
		return mav;
	}
	
	
    @RequestMapping(value="/aptCreate.do", method=RequestMethod.POST)
    private ModelAndView aptCreate(HttpServletRequest request,
            HttpServletResponse response) throws Exception {
    	request.setCharacterEncoding("utf-8");
    	ModelAndView mav = new ModelAndView();
    	try {
        	String cafeId = request.getParameter("cafeId");
        	String aptName = request.getParameter("aptName");
        	if(!cafeId.isEmpty() && !cafeId.isEmpty()) {
            	Apt apt = new Apt();
            	apt.setName(aptName);
            	apt.setCafeId(cafeId);
            	adminAptService.createAdminApt(apt);
        	}
    		List<Apt> list = adminAptService.searchAdminApt(1);
        	mav.setViewName("admin");
        	mav.addObject("Apts", list);
		} catch (Exception e) {
			// TODO: handle exception
		}
    	return mav;
    }
    
    @RequestMapping(value="/aptDelete.do", method = RequestMethod.GET)
    private ModelAndView aptDelete(HttpServletRequest request,
            HttpServletResponse response) throws Exception {
       	ModelAndView mav = new ModelAndView();
    	System.out.println("딜리트");
    	String aptId = request.getParameter("aptId");
    	System.out.println(aptId);
    	
    	adminAptService.deleteAdminAptById(aptId);
    	
		List<Apt> list = adminAptService.searchAdminApt(1);
    	mav.setViewName("admin");
    	mav.addObject("Apts", list);
    	return mav;
    }
    
    
    @RequestMapping(value="/aptSearch.do", method = RequestMethod.GET)
    private ModelAndView aptSearch(HttpServletRequest request,
            HttpServletResponse response) throws Exception {
    	request.setCharacterEncoding("utf-8");
    	
    	List<Apt> list = new ArrayList<>();
    	
       	ModelAndView mav = new ModelAndView();
       	String text = request.getParameter("aptSearchText");
    	System.out.println("/aptSearch.do");
    	System.out.println(text);
    	if(text == null) {
    		list = adminAptService.searchAdminApt(1);
    	}else {
           	list = adminAptService.searchAdminAptByName(text);
    	}
       	
    	mav.setViewName("admin");
    	mav.addObject("Apts", list);
    	return mav;
    	
    }
    
    
    
    
    
    

}
