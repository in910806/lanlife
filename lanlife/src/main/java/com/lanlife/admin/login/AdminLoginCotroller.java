package com.lanlife.admin.login;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.lanlife.admin.apt.service.AdminAptService;
import com.lanlife.admin.apt.service.impl.AdminAptServiceImpl;
import com.lanlife.domain.Apt;

@Controller
public class AdminLoginCotroller {
	
	//1. 비밀번호만 확인해서 관리자 페이지로 넘김.
	
	@Resource(name="adminAptService")
	private AdminAptServiceImpl adminAptService;
	
	@RequestMapping("/adminLogin")
	public String adminLogin(){
	  return "adminLogin";
	}
	
	@RequestMapping(value="/checkPwAdmin.do")
	private ModelAndView readApt (HttpServletRequest request,
            HttpServletResponse response){
		
		String inputPw = (String)request.getParameter("adminPw");
		System.out.println("비번"+inputPw);
		if(inputPw.equals("123123")) {
			
			List<Apt> list = adminAptService.searchAdminApt(1);
	    	ModelAndView mav = new ModelAndView();
	    	mav.setViewName("admin");
	    	mav.addObject("Apts", list);
			return mav;
		}else {
			return new ModelAndView("adminLogin");
		}
		
	}
	
	
	
	
	
	
	

}
